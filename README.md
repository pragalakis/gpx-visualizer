# [deprecated] gpx-visualizer

Moved to codeberg: https://codeberg.org/evasync/gpx-visualizer

### [Analysis Graphs](https://gitlab.com/pragalakis/gpx-visualizer/tree/master/analysis)

### [Poster Art](https://gitlab.com/pragalakis/gpx-visualizer/tree/master/posters)
