const canvasSketch = require('canvas-sketch');
const togeojson = require('@mapbox/togeojson');
const DOMParser = require('xmldom').DOMParser;
const load = require('load-asset');
const palette = require('./get-palette.js');

// Sketch parameters
const settings = {
  //dimensions: 'A3',
  dimensions: 'A6',
  //dimensions: '12r',
  pixelsPerInch: 300,
  units: 'cm'
};

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

const degreesToRadians = degrees => {
  return (degrees * Math.PI) / 180;
};

/**
 * Distance in km between earth coordinates
 * © Chris Veness, MIT-licensed,
 */
const distance = (lat1, lon1, lat2, lon2) => {
  const earthRadiusKm = 6371;

  let dLat = degreesToRadians(lat2 - lat1);
  let dLon = degreesToRadians(lon2 - lon1);

  lat1 = degreesToRadians(lat1);
  lat2 = degreesToRadians(lat2);

  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  return earthRadiusKm * c;
};

// Artwork function
const sketch = async () => {
  const GPX_FILE = './route.gpx';
  const text = await load({ url: GPX_FILE, type: 'text' });
  const gpx = new DOMParser().parseFromString(text);
  const converted = togeojson.gpx(gpx).features[0];
  const coords = converted.geometry.coordinates;

  // distance
  let totalDistance = 0;
  for (let i = 1; i < coords.length; i++) {
    totalDistance += distance(
      coords[i - 1][0],
      coords[i - 1][1],
      coords[i][0],
      coords[i][1]
    );
  }

  // date
  let DATE = new Date(converted.properties.time);
  const YEAR = DATE.getFullYear();
  const DATE_NUM = DATE.getDate();
  const MONTH = months[DATE.getMonth()];
  DATE = `${MONTH} ${DATE_NUM}, ${YEAR}`;

  const TITLE = 'ATHENS, GRE';
  const SUB_TITLE = 'ATHENS CLASSIC MARATHON';

  return ({ context, width, height }) => {
    // random color
    const colors = palette[Math.floor(Math.random() * palette.length)];

    // background
    context.fillStyle = colors[0];
    context.fillRect(0, 0, width, height);

    // line settings & colors
    context.lineWidth = 0.05;
    context.fillStyle = colors[1];
    context.strokeStyle = colors[1];

    // determing the limit box
    const minX = Math.min(...coords.map(v => v[0]));
    const maxX = Math.max(...coords.map(v => v[0]));
    const minY = Math.min(...coords.map(v => v[1]));
    const maxY = Math.max(...coords.map(v => v[1]));

    // Scaling our coordinates
    const zoom = 1.5;
    const xScale = width / Math.abs(maxX - minX);
    const yScale = height / Math.abs(maxY - minY);
    const scale = xScale < yScale ? xScale / zoom : yScale / zoom;

    const halfWidth = (width - (maxX - minX) * scale) / 2;
    const halfHeight = (height - (maxY - minY) * scale) / 2;

    // draw path
    context.beginPath();
    coords.forEach(v => {
      const x = (v[0] - minX) * scale;
      const y = (maxY - v[1]) * scale;
      context.lineTo(halfWidth + x, halfHeight / 2 + y);
      //context.strokeRect(halfWidth + x, halfHeight / 2 + y, 0.05, 0.05);
    });
    context.stroke();

    // draw text
    //let fontSize = 1.5;
    let fontSize = 0.6; // A6 paper size
    const font = 'Raleway';
    context.font = `${fontSize}pt ${font}`;
    context.fillText(
      TITLE,
      width / 2 - context.measureText(TITLE).width / 2,
      height - 3.5 * fontSize
    );

    //fontSize = 0.8;
    fontSize = 0.31; // A6 paper size
    context.font = `${fontSize}pt ${font}`;
    context.fillText(
      SUB_TITLE,
      width / 2 - context.measureText(SUB_TITLE).width / 2,
      height - 4.5 * fontSize
    );

    const dateDist = `${DATE} - ${totalDistance.toFixed(2)} K`;
    //fontSize = 0.5;
    fontSize = 0.2; // A6 paper size
    context.font = `${fontSize}pt ${font}`;
    context.fillText(
      dateDist,
      width / 2 - context.measureText(dateDist).width / 2,
      height - 4.5 * fontSize
    );
  };
};

// Start the sketch
canvasSketch(sketch, settings);
