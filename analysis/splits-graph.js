const canvasSketch = require('canvas-sketch');
const load = require('./load-gpx.js');
const mapRange = require('./map-range.js');
let stats = require('./stats.js');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  orientation: 'landscape',
  pixelsPerInch: 72,
  units: 'cm'
};

// Splits graph function
const sketch = async () => {
  const gpx = await load('./gpx/route.gpx');
  const coords = gpx.features[0].geometry.coordinates;
  const coordTimes = gpx.features[0].properties.coordTimes;
  stats = stats(coords, coordTimes);

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,98%)';
    context.fillRect(0, 0, width, height);

    // settings & colors
    context.lineWidth = 0.05;
    context.fillStyle = 'hsl(0,50%,50%)';
    context.strokeStyle = 'hsl(0,0%,0%)';
    const fontSize = 0.4;
    context.font = `${fontSize}pt Monospace`;

    const margin = 2;
    const inputMax = stats.slowestSplit[1];
    const inputMin = stats.fastestSplit[1];
    const pace = [];
    let px, py;

    context.fillText('(min/km)', margin - 1, margin - 1);
    context.fillText('(km)', width - margin, height - margin);

    stats.splits.forEach((split, i) => {
      let distance = split[0];
      if (distance < 1) {
        distance = split[0] + stats.splits[stats.splits.length - 2][0];
      }

      const x = mapRange(
        distance,
        0,
        stats.totalDistance,
        margin,
        width - margin * 2
      );

      const y = mapRange(
        split[1],
        inputMax,
        inputMin,
        margin,
        height - margin * 2
      );

      // previous x and y
      if (i === 0) {
        px = x;
        py = y;
      }

      // draw pace text
      const p = parseInt(split[1] * 100) / 100;
      if (!pace.includes(p)) {
        context.fillText(p, margin / 2, y + fontSize / 2);
        pace.push(p);
      }

      // draw distance text
      if (i === stats.splits.length - 1) {
        context.fillText(`${parseInt(distance * 100) / 100}`, x + 1, y);
      } else {
        context.fillText(
          `${parseInt(distance * 100) / 100}`,
          x,
          height - margin
        );
      }

      // draw circles
      if (i === 0 || i === stats.splits.length - 1) {
        context.beginPath();
        context.arc(x, y, 0.4, 0, Math.PI * 2, true);
        context.stroke();
      }

      context.beginPath();
      context.arc(x, y, 0.2, 0, Math.PI * 2, true);
      context.fill();

      // draw line
      context.beginPath();
      context.moveTo(px, py);
      context.lineTo(x, y);
      context.stroke();

      // previous x and y
      if (i > 0) {
        px = x;
        py = y;
      }
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
