const canvasSketch = require('canvas-sketch');
const load = require('./load-gpx.js');
const mapRange = require('./map-range.js');
let stats = require('./stats.js');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  orientation: 'landscape',
  pixelsPerInch: 72,
  units: 'cm'
};

// Splits bar chart function
const sketch = async () => {
  const gpx = await load('./gpx/route.gpx');
  const coords = gpx.features[0].geometry.coordinates;
  const coordTimes = gpx.features[0].properties.coordTimes;
  stats = stats(coords, coordTimes);

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,98%)';
    context.fillRect(0, 0, width, height);

    // settings & colors
    context.lineWidth = 0.05;
    context.fillStyle = 'hsl(0,0%,0%)';
    context.strokeStyle = 'hsl(0,0%,0%)';
    const fontSize = 0.5;
    context.font = `${fontSize}pt Monospace`;

    context.fillText('KM', 1, 1.5);
    context.fillText('PACE', 4, 1.5);
    context.fillText('ELEV', width - 4, 1.5);

    // separator
    context.beginPath();
    context.moveTo(1, 2);
    context.lineTo(width - 2, 2);
    context.stroke();

    context.lineWidth = 0.5;

    stats.splits.forEach((split, i) => {
      const x = 1;
      const y = 3 + 2 * fontSize * i;
      const distance = split[0];

      if (distance > 0.049) {
        // distance text
        context.fillText(parseInt(distance * 100) / 100, x, y);

        // pace text
        context.fillText(parseInt(split[1] * 100) / 100, 4 * x, y);

        // elevation text
        const ex = split[2] < 0 ? width - 4 - fontSize / 1.2 : width - 4;
        context.fillText(parseInt(split[2] * 10) / 10, ex, y);

        const inputMax = stats.slowestSplit[1];
        const inputMin = stats.fastestSplit[1];
        const outputMax = width - 7;
        const outputMin = 8 * x + 3;

        // bar chart color
        if (split[1] === inputMin || split[1] === inputMax) {
          context.strokeStyle = 'hsl(0,100%,70%)';
        } else {
          context.strokeStyle = 'hsl(0,100%,40%)';
        }

        // bar chart
        context.beginPath();
        context.moveTo(8 * x, y - fontSize / 2);
        context.lineTo(
          mapRange(split[1], inputMax, inputMin, outputMin, outputMax),
          y - fontSize / 2
        );
        context.stroke();
      }
    });
  };
};

// Start the sketch
canvasSketch(sketch, settings);
