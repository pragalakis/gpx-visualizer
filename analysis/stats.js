const getDistance = require('./get-distance');

module.exports = function stats(coords, coordTimes) {
  const pace = [];
  const time = [];
  const distance = [];
  const startTime = new Date(coordTimes[0]);
  const endTime = new Date(coordTimes[coordTimes.length - 1]);
  const totalDistance = getDistance(coords);
  const totalTime = (endTime - startTime) / 1000 / 60; //in minutes

  let uphill = 0;
  let downhill = 0;
  const splits = [];
  coords.forEach((v, i) => {
    //  elevation gain
    if (i != 0) {
      if (coords[i - 1][2] - v[2] < 0) {
        downhill += coords[i - 1][2] - v[2];
      } else {
        uphill += coords[i - 1][2] - v[2];
      }
    }

    // time, distance, pace
    const currentTime = (new Date(coordTimes[i]) - startTime) / 1000 / 60;
    const currentDistance = (totalDistance / coords.length) * i;
    const currentPace = currentTime / currentDistance;
    time.push(currentTime);
    distance.push(currentDistance);
    pace.push(currentPace);

    // splits
    const indx = Math.trunc(currentDistance);
    if (splits[indx] === undefined) splits.push([]);
    splits[indx].push([pace[i], v[2]]);
  });

  // fixing low pace at the start
  /* temp solution */
  pace[0] = pace[2];
  pace[1] = pace[2];
  splits[0][0][0] = pace[2];
  splits[0][1][0] = pace[2];

  // splits
  splits.forEach((split, i) => {
    const paceSum = split.reduce((acc, curVal) => acc + curVal[0], 0);

    let elGain = split.reduce((acc, curVal, j) => {
      if (j != 0) {
        return acc + curVal[1] - split[j - 1][1];
      } else {
        return acc;
      }
    }, 0);

    const splitDist = i === splits.length - 1 ? totalDistance - i : i + 1;
    // save the data to the splits array
    splits[i] = [splitDist, paceSum / split.length, elGain];
  });

  const fastestSplitPace = Math.min(...splits.map(v => v[1]));
  const slowestSplitPace = Math.max(...splits.map(v => v[1]));

  return {
    time: time,
    distance: distance,
    pace: pace,
    elevation: coords.map(v => v[2]),
    splits: splits, // [km,pace,elevation]
    fastestSplit: splits.find(v => v[1] === fastestSplitPace),
    slowestSplit: splits.find(v => v[1] === slowestSplitPace),
    startTime: startTime,
    endTime: endTime,
    minElevation: Math.min(...coords.map(v => v[2])),
    maxElevation: Math.max(...coords.map(v => v[2])),
    totalDistance: totalDistance,
    minPace: Math.min(...pace),
    maxPace: Math.max(...pace),
    averagePace: totalTime / totalDistance,
    averageSpeed: totalDistance / (totalTime * 60),
    uphill: uphill,
    downhill: downhill,
    elevationGain: uphill,
    elapsedTime: totalTime
  };
};
