module.exports = function axis(points, axisy, axisx, context) {
  const [spx, spy, vy2, hx2] = points;
  const [minvy, maxvy, stepY, flip, namey] = axisy;
  const [minvx, maxvx, stepX, namex] = axisx;

  const fontSize = 0.4;
  context.font = `${fontSize}pt Monospace`;
  context.strokeStyle = 'hsl(0,0%,80%)';

  // draw Y axis numbers and grid
  let texty = flip ? maxvy : minvy;
  for (let i = 0; i <= spy - vy2; i += (spy - vy2) / stepY) {
    context.beginPath();
    context.moveTo(spx, spy - i);
    context.lineTo(hx2, spy - i);
    context.stroke();

    context.fillText(
      `${parseInt(texty * 100) / 100} ${namey}`,
      spx - 7 * fontSize,
      spy - i + fontSize / 2
    );

    if (flip) {
      texty -= (maxvy - minvy) / stepY;
    } else {
      texty += (maxvy - minvy) / stepY;
    }
  }

  // draw X axis numbers and grid
  let textx = minvx;
  for (let i = 0; i <= hx2; i += (hx2 - spx) / stepX) {
    context.beginPath();
    context.moveTo(spx + i, spy);
    context.lineTo(spx + i, vy2);
    context.stroke();

    context.fillText(
      `${parseInt(textx * 100) / 100} ${namex}`,
      spx + i - 0.5,
      spy + 3 * fontSize
    );
    textx += maxvx / stepX;
  }

  // draw axis box
  context.strokeStyle = 'hsl(0,0%,6%)';
  context.strokeRect(spx, vy2, hx2 - spx, spy - vy2);

  /*
  // draw Y axis
  context.beginPath();
  context.moveTo(spx, spy);
  context.lineTo(spx, vy2);
  context.stroke();

  // draw X axis
  context.beginPath();
  context.moveTo(spx, spy);
  context.lineTo(hx2, spy);
  context.stroke();
  */
};
