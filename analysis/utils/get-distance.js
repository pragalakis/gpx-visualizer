const degreesToRadians = degrees => {
  return (degrees * Math.PI) / 180;
};

/**
 * Distance in km between earth coordinates
 * © Chris Veness, MIT-licensed,
 */
function distance(lat1, lon1, lat2, lon2) {
  const earthRadiusKm = 6371;

  let dLat = degreesToRadians(lat2 - lat1);
  let dLon = degreesToRadians(lon2 - lon1);

  lat1 = degreesToRadians(lat1);
  lat2 = degreesToRadians(lat2);

  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  return earthRadiusKm * c;
}

module.exports = function totalDistance(coords) {
  let tDistance = 0;
  for (let i = 1; i < coords.length; i++) {
    tDistance += distance(
      coords[i - 1][0],
      coords[i - 1][1],
      coords[i][0],
      coords[i][1]
    );
  }
  return tDistance;
};
