const togeojson = require('@mapbox/togeojson');
const DOMParser = require('xmldom').DOMParser;
const load = require('load-asset');

module.exports = async function load_gpx(GPX_FILE) {
  const text = await load({ url: GPX_FILE, type: 'text' });
  const gpx = new DOMParser().parseFromString(text);
  return togeojson.gpx(gpx);
};
