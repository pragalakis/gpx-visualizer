## Elevation Graph

<img src="./screenshots/elevation-graph.png" width="600">


## Pace Graph

<img src="./screenshots/pace-graph.png" width="600">


## Splits Bar Chart

<img src="./screenshots/splits-bar-chart.png" width="600">


## Splits Graph 

<img src="./screenshots/splits-graph.png" width="600">
