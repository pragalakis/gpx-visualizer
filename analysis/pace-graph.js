const canvasSketch = require('canvas-sketch');
const load = require('./load-gpx.js');
const drawAxis = require('./draw-axis.js');
let stats = require('./stats.js');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  orientation: 'landscape',
  pixelsPerInch: 72,
  units: 'cm'
};

// Pace graph function
const sketch = async () => {
  const gpx = await load('./route.gpx');
  const coords = gpx.features[0].geometry.coordinates;
  const coordTimes = gpx.features[0].properties.coordTimes;
  stats = stats(coords, coordTimes);
  const flip = true;

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,98%)';
    context.fillRect(0, 0, width, height);

    // line settings & colors
    context.lineWidth = 0.05;
    context.fillStyle = 'hsl(0,0%,0%)';
    context.strokeStyle = 'rgb(255,0,0)';

    // determing the limit box
    const minX = 3;
    const maxX = width - 6;
    const minY = stats.minPace;
    const maxY = stats.maxPace;

    // Scaling our coordinates
    const zoom = 1.4;
    const yScale = height / Math.abs(maxY - minY);
    const scale = yScale / zoom;

    // draw graph
    const graph = [];
    context.beginPath();
    coords.forEach((v, i) => {
      const x = minX + i / (coords.length / maxX);
      if (flip) {
        var y = height - 4 - (maxY - stats.pace[i]) * scale;
      } else {
        var y = 4 + (maxY - stats.pace[i]) * scale;
      }
      context.lineTo(x, y);
      graph.push([x, y, v[2]]);
    });
    context.stroke();

    // draw axis and numbers
    const endVertY = 4;
    const endHorX = graph[0][0] + maxX;
    const stepX = 10;
    const stepY = 15;
    drawAxis(
      [graph[0][0], height - endVertY, endVertY, endHorX],
      [minY, maxY, stepY, flip, '/km'],
      [0, stats.totalDistance, stepX, 'km'],
      context
    );

    // dashed line for average pace
    const ap = flip
      ? 4 - (minY - stats.averagePace) * scale
      : 4 + (maxY - stats.averagePace) * scale;
    context.setLineDash([0.5, 0.5]);
    context.beginPath();
    context.moveTo(graph[0][0], ap);
    context.lineTo(graph[0][0] + maxX, ap);
    context.stroke();
  };
};

// Start the sketch
canvasSketch(sketch, settings);
