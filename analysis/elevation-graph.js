const canvasSketch = require('canvas-sketch');
const load = require('./load-gpx.js');
const drawAxis = require('./draw-axis.js');
let stats = require('./stats.js');

// Sketch parameters
const settings = {
  dimensions: 'A3',
  orientation: 'landscape',
  pixelsPerInch: 72,
  units: 'cm'
};

// Graph function
const sketch = async ({ update }) => {
  const gpx = await load('./route.gpx');
  const coords = gpx.features[0].geometry.coordinates;
  const coordTimes = gpx.features[0].properties.coordTimes;
  stats = stats(coords, coordTimes);
  const flip = false;

  // interactivity
  let xp = 0;
  let yp = 0;
  let textElevation = '';
  let textDistance = '';
  let textPace = '';
  // get the coords of the cursor
  document.onmousemove = function move(e) {
    xp = e.clientX || 0;
    yp = e.clientY || 0;
    update();
  };

  return ({ context, width, height, styleWidth, styleHeight }) => {
    // background
    context.fillStyle = 'hsl(0,0%,98%)';
    context.fillRect(0, 0, width, height);

    // line settings & colors
    context.lineWidth = 0.05;
    context.fillStyle = 'hsl(0,0%,0%)';
    context.strokeStyle = 'rgb(255,0,0)';

    // determing the limit box
    const minX = 3;
    const maxX = width - 6;
    const minY = stats.minElevation;
    const maxY = stats.maxElevation;

    // Scaling our coordinates
    const zoom = 1.4;
    const yScale = height / Math.abs(maxY - minY);
    const scale = yScale / zoom;

    // draw graph
    const graph = [];
    context.beginPath();
    coords.forEach((v, i) => {
      const x = minX + i / (coords.length / maxX);
      const y = 4 + (maxY - v[2]) * scale;
      context.lineTo(x, y);
      graph.push([x, y, v[2]]);
    });
    context.stroke();

    // draw axis and numbers
    const endVertY = 4;
    const endHorX = graph[0][0] + maxX;
    const stepX = 5;
    const stepY = 5;
    drawAxis(
      [graph[0][0], graph[0][1], endVertY, endHorX],
      [minY, maxY, stepY, flip, 'm'],
      [0, stats.totalDistance, stepX, 'km'],
      context
    );

    // interactivity
    context.lineWidth = 0.05;
    xp = (xp / styleWidth) * width;
    yp = (yp / styleHeight) * height;
    // depending on mouse location - find the nearest point on the graph
    const dis = [];
    graph.map(v => dis.push(Math.hypot(xp - v[0], yp - v[1])));
    const min = Math.min(...dis);
    const x = minX + dis.indexOf(min) / (graph.length / maxX);
    const y = 4 + (maxY - graph[dis.indexOf(min)][2]) * scale;

    // draw lines and rect on graph
    context.fillRect(x - 0.2 / 2, y - 0.2 / 2, 0.2, 0.2);
    context.beginPath();
    context.moveTo(graph[0][0], y);
    context.lineTo(endHorX, y);
    context.stroke();

    context.beginPath();
    context.moveTo(x, graph[0][1]);
    context.lineTo(x, endVertY);
    context.stroke();

    // draw separator
    context.beginPath();
    context.moveTo(0, height - 2.2);
    context.lineTo(width, height - 2.2);
    context.stroke();

    // draw stats
    const fontSize = 0.5;
    context.font = `${fontSize}pt Monospace`;

    // draw current elevation
    textElevation = graph[dis.indexOf(min)][2];
    context.fillText(`Elevation: ${textElevation} meters`, 2, height - 0.8);

    // draw current distance
    textDistance = stats.distance[dis.indexOf(min)];
    context.fillText(`${textDistance.toFixed(2)} km`, 18, height - 0.8);

    // draw current time
    const currentTime = stats.time[dis.indexOf(min)];
    context.fillText(`${currentTime.toFixed(2)}`, x, endVertY - 0.5);

    // draw current pace
    textPace = stats.pace[dis.indexOf(min)];
    context.fillText(`Pace: ${textPace.toFixed(1)} km/min`, 32, height - 0.8);
  };
};

// Start the sketch
canvasSketch(sketch, settings);
